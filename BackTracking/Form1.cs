﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BackTracking
{
    using TSet = SortedSet<int>;
    public partial class Form1 : Form
    {
        int n;
        int[,] matr; //матрица смежности
        int length;  //длина текущего пути
        int len;     //длина всего пути
        int lenOpt;  //длина оптимального пути
        public Form1()
        {
            InitializeComponent();
        }

        private void загрузитьИзToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog op = new OpenFileDialog();
            op.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            if (op.ShowDialog() == DialogResult.OK)
            {
                string[] mass = File.ReadAllLines(op.FileName);
                n = Convert.ToInt32(mass[0]);
                matr = new int[n, n];
                for (int i = 0; i < n; i++)
                {
                    int[] m = mass[i + 1].Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).Select(s => int.Parse(s)).ToArray();
                    for (int j = 0; j < m.Length; j++)
                    {
                        matr[i, j] = m[j];
                    }
                }
            }
            dataGridView1.RowCount = n;
            dataGridView1.ColumnCount = n;
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    dataGridView1.Rows[i].Cells[j].Value = matr[i, j];
                    dataGridView1.Columns[j].Width = 60;
                }
            }
        }

        private void findShortPath(ref int t, ref TSet visited, int city)
        {
            if (t == city)  //если мы добрались до нужного города
            {
                //проверка оптимальности
                if (length < lenOpt)
                {
                    lenOpt = length;
                }
            }
            else
            {
                for (int k = 0; k < n; k++)
                {
                    //выбор города - k
                    //если можем попасть в этот город
                    int check;
                    if (matr[t, k]!= 0 && !visited.TryGetValue(k, out check))
                    {
                        //включение
                        length += matr[t, k];
                        visited.Add(k);

                        findShortPath(ref k, ref visited, city);

                        //исключение города на подъеме рекурсии
                        visited.Remove(k);
                        length -= matr[t, k];
                    }

                }
            }
        }
        //Нахождение расстояний до других городов
        private int findAllPaths(int t, ref TSet v)
        {
            for (int l = 0; l < n; l++)
            {
                if (matr[t, l] != 0 && t != l)
                {
                    lenOpt = int.MaxValue;
                    findShortPath(ref t, ref v, l);
                    len += lenOpt;
                }
            }
            return len;
        }
        //Нахождение города с минимальным расстоянием
        private void mainShortPaths(ref int minRoadsLen, ref int minCity, int a)
        {
            length = 0;
            len = 0;
            TSet v = new TSet();
            lenOpt = int.MaxValue;
            v.Add(a);

            int RoadsWay = findAllPaths(a, ref v);
            if (RoadsWay < minRoadsLen)
            {
                minRoadsLen = RoadsWay;
                minCity = a + 1;
            }
            string s = "От " + (a + 1).ToString() + "-го города до остальных - " + RoadsWay.ToString();
            richTextBox1.AppendText(s);
            richTextBox1.AppendText("\n");
        }

        private void выполнитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int minRoadsLen = int.MaxValue;
            int minCity = 0;
            int a;

            for (a = 0; a < n; a++)
                mainShortPaths(ref minRoadsLen, ref minCity, a);
            MessageBox.Show("Город с минимальным расстоянием до остальных — " + minCity.ToString());
        }
    }
}
